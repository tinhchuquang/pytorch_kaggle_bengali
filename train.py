import gc

import numpy as np
import pandas as pd
import sklearn.metrics
import torch
import torch.nn as nn
from sklearn.model_selection import train_test_split

from generator_data import load_data, GraphemeDataset
from model import EfficientNet
from onecyclelr import OneCycleLR
from optimizer_code import Over9000

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
print(device)
model = EfficientNet.from_pretrained('efficientnet-b7').to(device)
model.load_state_dict(torch.load('effnetb7_trial_stage1.pth'))
n_epochs = 1
optimizer = Over9000(model.parameters(), lr=2e-3, weight_decay=1e-3)
# scheduler = torch.optim.lr_scheduler.OneCycleLR(optimizer, 1e-2, total_steps=None, epochs=n_epochs,
#                                                 steps_per_epoch=5021, pct_start=0.0, anneal_strategy='cos',
#                                                 cycle_momentum=True, base_momentum=0.85, max_momentum=0.95,
#                                                 div_factor=100.0)
scheduler = OneCycleLR(optimizer,
                       num_steps=1000,
                       lr_range=(1e-5, 1e-2),
                       momentum_range=(0.85, 0.95),
                       annihilation_frac=0.1,
                       reduce_factor=0.01,
                       last_step=-1)
criterion = nn.CrossEntropyLoss()
batch_size = 32
train, data_full = load_data()
train_df, valid_df = train_test_split(train, test_size=0.2, random_state=42, shuffle=True)
data_train_df, data_valid_df = train_test_split(data_full, test_size=0.2, random_state=42, shuffle=True)
del data_full
gc.collect()

train_dataset = GraphemeDataset(data_train_df, train_df, transform=True)
valid_dataset = GraphemeDataset(data_valid_df, valid_df, transform=False)
torch.cuda.empty_cache()
gc.collect()

train_loader = torch.utils.data.DataLoader(train_dataset, batch_size=batch_size, shuffle=True)
valid_loader = torch.utils.data.DataLoader(valid_dataset, batch_size=batch_size, shuffle=True)


# metric accuaracy
def macro_recall(pred_y, y, n_grapheme=168, n_vowel=11, n_consonant=7):
    pred_y = torch.split(pred_y, [n_grapheme, n_vowel, n_consonant], dim=1)
    pred_labels = [torch.argmax(py, dim=1).cpu().numpy() for py in pred_y]

    y = y.cpu().numpy()
    # pred_y = [p.cpu().numpy() for p in pred_y]

    recall_grapheme = sklearn.metrics.recall_score(pred_labels[0], y[:, 0], average='macro')
    recall_vowel = sklearn.metrics.recall_score(pred_labels[1], y[:, 1], average='macro')
    recall_consonant = sklearn.metrics.recall_score(pred_labels[2], y[:, 2], average='macro')
    scores = [recall_grapheme, recall_vowel, recall_consonant]
    final_score = np.average(scores, weights=[2, 1, 1])
    # print(f'recall: grapheme {recall_grapheme}, vowel {recall_vowel}, consonant {recall_consonant}, '
    #       f'total {final_score}, y {y.shape}')
    return final_score


def macro_recall_multi(pred_graphemes, true_graphemes, pred_vowels, true_vowels, pred_consonants, true_consonants,
                       n_grapheme=168, n_vowel=11, n_consonant=7):
    # pred_y = torch.split(pred_y, [n_grapheme], dim=1)
    pred_label_graphemes = torch.argmax(pred_graphemes, dim=1).cpu().numpy()

    true_label_graphemes = true_graphemes.cpu().numpy()

    pred_label_vowels = torch.argmax(pred_vowels, dim=1).cpu().numpy()

    true_label_vowels = true_vowels.cpu().numpy()

    pred_label_consonants = torch.argmax(pred_consonants, dim=1).cpu().numpy()

    true_label_consonants = true_consonants.cpu().numpy()
    # pred_y = [p.cpu().numpy() for p in pred_y]

    recall_grapheme = sklearn.metrics.recall_score(pred_label_graphemes, true_label_graphemes, average='macro')
    recall_vowel = sklearn.metrics.recall_score(pred_label_vowels, true_label_vowels, average='macro')
    recall_consonant = sklearn.metrics.recall_score(pred_label_consonants, true_label_consonants, average='macro')
    scores = [recall_grapheme, recall_vowel, recall_consonant]
    final_score = np.average(scores, weights=[2, 1, 1])
    # print(f'recall: grapheme {recall_grapheme}, vowel {recall_vowel}, consonant {recall_consonant}, '
    #       f'total {final_score}')
    return final_score


def calc_macro_recall(solution, submission):
    # solution df, submission df
    scores = []
    for component in ['grapheme_root', 'consonant_diacritic', 'vowel_diacritic']:
        y_true_subset = solution[solution[component] == component]['target'].values
        y_pred_subset = submission[submission[component] == component]['target'].values
        scores.append(sklearn.metrics.recall_score(
            y_true_subset, y_pred_subset, average='macro'))
    final_score = np.average(scores, weights=[2, 1, 1])
    return final_score


def train(epoch, history):
    model.train()
    losses = []
    accs = []
    acc = 0.0
    total = 0.0
    running_loss = 0.0
    running_acc = 0.0
    running_recall = 0.0
    for idx, (inputs, labels1, labels2, labels3) in enumerate(train_loader):
        inputs = inputs.to(device)
        labels1 = labels1.to(device)
        labels2 = labels2.to(device)
        labels3 = labels3.to(device)
        total += len(inputs)
        optimizer.zero_grad()
        outputs1, outputs2, outputs3 = model(inputs.unsqueeze(1).float())
        loss1 = criterion(outputs1, labels1)
        loss2 = criterion(outputs2, labels2)
        loss3 = criterion(outputs3, labels3)
        running_loss += loss1.item() + loss2.item() + loss3.item()
        running_recall += macro_recall_multi(outputs2, labels2, outputs1, labels1, outputs3, labels3)
        running_acc += (outputs1.argmax(1) == labels1).float().mean()
        running_acc += (outputs2.argmax(1) == labels2).float().mean()
        running_acc += (outputs3.argmax(1) == labels3).float().mean()
        (loss1 + loss2 + loss3).backward()
        optimizer.step()
        optimizer.zero_grad()
        acc = running_acc / total
        # scheduler.step()
    losses.append(running_loss / len(train_loader))
    accs.append(running_acc / (len(train_loader) * 3))
    print(' train epoch : {}\tacc : {:.2f}%'.format(epoch, running_acc / (len(train_loader) * 3)))
    print('loss : {:.4f}'.format(running_loss / len(train_loader)))

    print('recall: {:.4f}'.format(running_recall / len(train_loader)))
    total_train_recall = running_recall / len(train_loader)
    torch.cuda.empty_cache()
    gc.collect()
    history.loc[epoch, 'train_loss'] = losses[0]
    history.loc[epoch, 'train_acc'] = accs[0].cpu().numpy()
    history.loc[epoch, 'train_recall'] = total_train_recall
    return total_train_recall


def evaluate(epoch, history):
    model.eval()
    losses = []
    accs = []
    recalls = []
    acc = 0.0
    total = 0.0
    # print('epochs {}/{} '.format(epoch+1,epochs))
    running_loss = 0.0
    running_acc = 0.0
    running_recall = 0.0
    with torch.no_grad():
        for idx, (inputs, labels1, labels2, labels3) in enumerate(valid_loader):
            inputs = inputs.to(device)
            labels1 = labels1.to(device)
            labels2 = labels2.to(device)
            labels3 = labels3.to(device)
            total += len(inputs)
            outputs1, outputs2, outputs3 = model(inputs.unsqueeze(1).float())
            loss1 = criterion(outputs1, labels1)
            loss2 = 2 * criterion(outputs2, labels2)
            loss3 = criterion(outputs3, labels3)
            running_loss += loss1.item() + loss2.item() + loss3.item()
            running_recall += macro_recall_multi(outputs2, labels2, outputs1, labels1, outputs3, labels3)
            running_acc += (outputs1.argmax(1) == labels1).float().mean()
            running_acc += (outputs2.argmax(1) == labels2).float().mean()
            running_acc += (outputs3.argmax(1) == labels3).float().mean()
            acc = running_acc / total
            scheduler.step()
    losses.append(running_loss / len(valid_loader))
    accs.append(running_acc / (len(valid_loader) * 3))
    recalls.append(running_recall / len(valid_loader))
    total_recall = running_recall / len(valid_loader)
    print('val epoch: {} \tval acc : {:.2f}%'.format(epoch, running_acc / (len(valid_loader) * 3)))
    print('loss : {:.4f}'.format(running_loss / len(valid_loader)))
    print('recall: {:.4f}'.format(running_recall / len(valid_loader)))
    history.loc[epoch, 'valid_loss'] = losses[0]
    history.loc[epoch, 'valid_acc'] = accs[0].cpu().numpy()
    history.loc[epoch, 'valid_recall'] = total_recall
    return total_recall


if __name__ == '__main__':
    history = pd.DataFrame()
    n_epochs = 200
    valid_recall = 0.0
    best_valid_recall = 0.0
    for epoch in range(n_epochs):
        print('Epoch:', epoch + 1)
        torch.cuda.empty_cache()
        gc.collect()
        train_recall = train(epoch, history)
        valid_recall = evaluate(epoch, history)
        if valid_recall > best_valid_recall:
            print(
                f'Validation recall has increased from:  {best_valid_recall:.4f} to: {valid_recall:.4f}. Saving checkpoint')
            torch.save(model.state_dict(), 'effnetb7_trial_stage1.pth')
            best_valid_recall = valid_recall
