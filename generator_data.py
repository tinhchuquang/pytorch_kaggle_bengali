import gc
import warnings

import cv2
import numpy as np
import pandas as pd
import torch
import torch.nn as nn
## This library is for augmentations .
from albumentations import (
    Compose,
    ElasticTransform,
    GridDistortion,
    OpticalDistortion,
    OneOf,
    RandomGamma,
    ShiftScaleRotate,
    GaussNoise,
    Blur,
    GaussianBlur,
)
from torch.functional import F
from torch.utils.data import Dataset

from data_setup import PATH

warnings.filterwarnings('ignore')

DATA_PATH = '/data/tinhcq/bengaliai-cv19/'


def load_data():
    train = pd.read_csv(PATH + "train.csv")
    data0 = pd.read_feather(DATA_PATH + "train_data_00_l.feather")
    data1 = pd.read_feather(DATA_PATH + 'train_data_11_l.feather')
    data2 = pd.read_feather(DATA_PATH + 'train_data_22_l.feather')
    data3 = pd.read_feather(DATA_PATH + 'train_data_33_l.feather')
    data_full = pd.concat([data0, data1, data2, data3], ignore_index=True)
    del data0, data1, data2, data3
    gc.collect()
    print(data_full.shape)
    return train, data_full


class Cutout:
    def __init__(self, mask_size, p, cutout_inside, mask_color=1):
        self.p = p
        self.mask_size = mask_size
        self.cutout_inside = cutout_inside
        self.mask_color = mask_color

        self.mask_size_half = mask_size // 2
        self.offset = 1 if mask_size % 2 == 0 else 0

    def __call__(self, image):
        image = np.asarray(image).copy()

        if np.random.random() > self.p:
            return image

        h, w = image.shape[:2]

        if self.cutout_inside:
            cxmin, cxmax = self.mask_size_half, w + self.offset - self.mask_size_half
            cymin, cymax = self.mask_size_half, h + self.offset - self.mask_size_half
        else:
            cxmin, cxmax = 0, w + self.offset
            cymin, cymax = 0, h + self.offset

        cx = np.random.randint(cxmin, cxmax)
        cy = np.random.randint(cymin, cymax)
        xmin = cx - self.mask_size_half
        ymin = cy - self.mask_size_half
        xmax = xmin + self.mask_size
        ymax = ymin + self.mask_size
        xmin = max(0, xmin)
        ymin = max(0, ymin)
        xmax = min(w, xmax)
        ymax = min(h, ymax)
        image[ymin:ymax, xmin:xmax] = self.mask_color
        return image


class DualCutout:
    def __init__(self, mask_size, p, cutout_inside, mask_color=1):
        self.cutout = Cutout(mask_size, p, cutout_inside, mask_color)

    def __call__(self, image):
        return np.hstack([self.cutout(image), self.cutout(image)])


class DualCutoutCriterion:
    def __init__(self, alpha):
        self.alpha = alpha
        self.criterion = nn.CrossEntropyLoss(reduction='mean')

    def __call__(self, preds, targets):
        preds1, preds2 = preds
        return (self.criterion(preds1, targets) + self.criterion(
            preds2, targets)) * 0.5 + self.alpha * F.mse_loss(preds1, preds2)


def mixup(data, targets, alpha, n_classes):
    indices = torch.randperm(data.size(0))
    shuffled_data = data[indices]
    shuffled_targets = targets[indices]

    lam = np.random.beta(alpha, alpha)
    data = data * lam + shuffled_data * (1 - lam)
    targets = (targets, shuffled_targets, lam)

    return data, targets


def mixup_criterion(preds, targets):
    targets1, targets2, lam = targets
    criterion = nn.CrossEntropyLoss(reduction='mean')
    return lam * criterion(preds, targets1) + (1 - lam) * criterion(
        preds, targets2)


class RandomErasing:
    def __init__(self, p, area_ratio_range, min_aspect_ratio, max_attempt):
        self.p = p
        self.max_attempt = max_attempt
        self.sl, self.sh = area_ratio_range
        self.rl, self.rh = min_aspect_ratio, 1. / min_aspect_ratio

    def __call__(self, image):
        image = np.asarray(image).copy()

        if np.random.random() > self.p:
            return image

        h, w = image.shape[:2]
        image_area = h * w

        for _ in range(self.max_attempt):
            mask_area = np.random.uniform(self.sl, self.sh) * image_area
            aspect_ratio = np.random.uniform(self.rl, self.rh)
            mask_h = int(np.sqrt(mask_area * aspect_ratio))
            mask_w = int(np.sqrt(mask_area / aspect_ratio))

            if mask_w < w and mask_h < h:
                x0 = np.random.randint(0, w - mask_w)
                y0 = np.random.randint(0, h - mask_h)
                x1 = x0 + mask_w
                y1 = y0 + mask_h
                image[y0:y1, x0:x1] = np.random.uniform(0, 1)
                break

        return image

    # Add augmentation


train_aug = Compose([
    ShiftScaleRotate(p=1, border_mode=cv2.BORDER_CONSTANT, value=1),
    OneOf([
        ElasticTransform(p=0.1, alpha=1, sigma=50, alpha_affine=50, border_mode=cv2.BORDER_CONSTANT, value=1),
        GridDistortion(distort_limit=0.05, border_mode=cv2.BORDER_CONSTANT, value=1, p=0.1),
        OpticalDistortion(p=0.1, distort_limit=0.05, shift_limit=0.2, border_mode=cv2.BORDER_CONSTANT, value=1)
    ], p=0.3),
    OneOf([
        # GaussNoise(var_limit=1.0),
        Blur(),
        GaussianBlur(blur_limit=3)
    ], p=0.4),
    RandomGamma(p=0.8)])


class ToTensor:
    def __call__(self, data):
        if isinstance(data, tuple):
            return tuple([self._to_tensor(image) for image in data])
        else:
            return self._to_tensor(data)

    def _to_tensor(self, data):
        if len(data.shape) == 3:
            return torch.from_numpy(data.transpose(2, 0, 1).astype(np.float32))
        else:
            return torch.from_numpy(data[None, :, :].astype(np.float32))


class Normalize:
    def __init__(self, mean, std):
        self.mean = np.array(mean)
        self.std = np.array(std)

    def __call__(self, image):
        image = np.asarray(image).astype(np.float32) / 255.
        image = (image - self.mean) / self.std
        return image


class GraphemeDataset(Dataset):
    def __init__(self, df, label, _type='train', transform=True, aug=train_aug):
        self.df = df
        self.label = label
        self.aug = aug
        self.transform = transform
        self.data = df.iloc[:, 1:].values

    def __len__(self):
        return len(self.df)

    def __getitem__(self, idx):
        label1 = self.label.vowel_diacritic.values[idx]
        label2 = self.label.grapheme_root.values[idx]
        label3 = self.label.consonant_diacritic.values[idx]
        # image = self.df.iloc[idx][1:].values.reshape(128,128).astype(np.float)
        image = self.data[idx, :].reshape(128, 128).astype(np.float)
        if self.transform:
            augment = self.aug(image=image)
            image = augment['image']
            cutout = Cutout(32, 0.5, True, 1)
            image = cutout(image)
        norm = Normalize([0.0692], [0.2051])
        image = norm(image)

        return image, label1, label2, label3
