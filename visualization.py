import gc

import matplotlib.pyplot as plt
import torch
from sklearn.model_selection import train_test_split
import numpy as np
from generator_data import load_data, GraphemeDataset
from tqdm import tqdm

train, data_full = load_data()
train_df, valid_df = train_test_split(train, test_size=0.2, random_state=42, shuffle=True)
data_train_df, data_valid_df = train_test_split(data_full, test_size=0.2, random_state=42, shuffle=True)
del data_full
gc.collect()

train_dataset = GraphemeDataset(data_train_df, train_df, transform=True)
valid_dataset = GraphemeDataset(data_valid_df, valid_df, transform=False)
torch.cuda.empty_cache()
gc.collect()


def visualize(original_image, aug_image):
    fontsize = 18

    f, ax = plt.subplots(1, 2, figsize=(8, 8))

    ax[0].imshow(original_image, cmap='gray')
    ax[0].set_title('Original image', fontsize=fontsize)
    ax[1].imshow(aug_image, cmap='gray')
    ax[1].set_title('Augmented image', fontsize=fontsize)


# orig_image = data_train_df.iloc[0, 1:].values.reshape(128, 128).astype(np.float)
# aug_image = train_dataset[0][0]
#
# for i in range(20):
#     aug_image = train_dataset[0][0]
#     visualize(orig_image, aug_image)
#
# for i in range(len(train_dataset)):
#     print(train_dataset[i][1], train_dataset[i][2], train_dataset[i][3])

train_loader = torch.utils.data.DataLoader(train_dataset, batch_size=1, shuffle=True)
valid_loader = torch.utils.data.DataLoader(valid_dataset, batch_size=1, shuffle=True)

for idx, (inputs, labels1, labels2, labels3) in tqdm(enumerate(train_loader), total=len(train_loader)):
    print('labels1, labels2, labels3')